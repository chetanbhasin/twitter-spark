package helper

import actors._
import play.api.libs.json._

/**
 * Object containing JSON Writes
 */
object JsonRes {

  // Convert BuyIntent data to JSON
  private implicit val buyIntentWrites = new Writes[BuyIntent] {
    def writes(intent: BuyIntent) = Json.obj(
      "pos" -> intent.pos.toLong,
      "neg" -> intent.neg.toLong,
      "neu" -> intent.neu.toLong
    )
  }

  // Convert RecommendIntent data to JSON
  private implicit val rIntentWrites = new Writes[RecommendIntent] {
    def writes(intent: RecommendIntent) = Json.obj(
      "pos" -> intent.pos.toLong,
      "neg" -> intent.neg.toLong,
      "neu" -> intent.neu.toLong
    )
  }

  // Convert NoIntent data to JSON
  private implicit val noIntentWrites = new Writes[NoIntent] {
    def writes(intent: NoIntent) = Json.obj(
      "pos" -> intent.pos.toLong,
      "neg" -> intent.neg.toLong,
      "neu" -> intent.neg.toLong
    )
  }

  // Convert a country value data to JSON
  private implicit val countryValWrites = new Writes[CountryValue] {
    def writes(country: CountryValue) = Json.obj(
      "country" -> country.country,
      "number" -> country.number.toLong
    )
  }

  // Convert a contry value data in a list to JSON array
  private implicit val countryWrites = new Writes[List[CountryValue]] {
    def writes(countries: List[CountryValue]) = Json.arr(
      countries.map(Json.toJson(_))
    )
  }

  // Convert a Map[String, Long] to JSON Object for word count
  private implicit val wordCount = new Writes[List[(String, Long)]] {
    def writes(wc: List[(String, Long)]) = JsObject(
      wc.toSeq.map(i => i._1 -> JsNumber(i._2))
    )
  }

  // Create a JSON res to be sent when asked for updates
  implicit val updatesWrites = new Writes[Updates] {
    def writes(update: Updates) = Json.obj(
      "running" -> update.running,
      "buyIntent" -> Json.toJson(update.buyIntent),
      "rIntent" -> Json.toJson(update.rIntent),
      "noIntent" -> Json.toJson(update.noIntent),
      "topCountires" -> Json.toJson(update.topCountires),
      "wordCount" -> JsNumber(update.wc),
      "numTweets" -> JsNumber(update.numTweets)
    )
  }

  implicit val nothingWrites = new Writes[Unit] {
    def writes(nothing: Unit) = Json.obj(
      "output" -> "Operation executed."
    )
  }

}
