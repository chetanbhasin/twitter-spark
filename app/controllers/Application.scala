package controllers

import actors._
import akka.pattern.ask
import akka.util.Timeout
import helper.JsonRes._
import play.api.libs.json._
import play.api.mvc._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration._

class Application extends Controller {

  implicit val timeout = Timeout(60 seconds)

  def index = getUpdates

  def getUpdates = Action.async {
    val update: Future[Updates] = (infoManager ? GetUdpates).asInstanceOf[Future[Updates]]
    update map {
      (item: Updates) =>
        Ok(Json.toJson(item))
    }
  }

  tweetHandler ! StartWorker

}
