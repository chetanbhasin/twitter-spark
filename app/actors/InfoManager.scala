package actors

import akka.actor.{Actor, ActorSystem, Props}

case object GetUdpates

case class Updates(buyIntent: BuyIntent, rIntent: RecommendIntent, noIntent: NoIntent, topCountires: List[CountryValue], running: Boolean, wc: Long, numTweets: Long)

case class CountryValue(country: String, number: BigInt)

// Intent holders
trait IntentData

class BuyIntent(var pos: BigInt = 0, var neg: BigInt = 0, var neu: BigInt = 0) extends IntentData

class RecommendIntent(var pos: BigInt = 0, var neg: BigInt = 0, var neu: BigInt = 0) extends IntentData

class NoIntent(var pos: BigInt = 0, var neg: BigInt = 0, var neu: BigInt = 0) extends IntentData

// Companion object for InfoManager
object InfoManager {

  /**
   * Apply method for companion object InforManager
   * @param actorName Name of the actor (default: Information Manager)
   * @return ActorRef[InfoManager]
   */
  def apply(system: ActorSystem, actorName: String = "InformationManager") = {
    system.actorOf(Props[InfoManager], name = actorName)
  }

}

/**
 * Actor that manages all the information regarding the system
 */
class InfoManager extends Actor {

  // Define an actor to be used later
  //private val tweetHandler = context.actorOf(Props[TweetHandler], name="TweetHandler")

  // Note weather the system is currently in running state
  private var running = false

  // Number of tweets
  private var numTweets = 0.toLong

  // Word count
  private var wordCount = 0.toLong

  // Countries
  private var countries = scala.collection.mutable.Map[String, BigInt]()

  // Intents weight
  private val buyIntent = new BuyIntent
  private val recommendIntent = new RecommendIntent
  private val noIntent = new NoIntent

  /**
   * Get top countries from the list of existing countries
   * @param n Number of countires to get
   * @return List[(String, BigInt)]
   */
  private def getTopCountires(n: Int): List[CountryValue] = countries.toList sortBy (_._2) take n map {
    element =>
      CountryValue(element._1, element._2)
  }


  /**
   * Process and store results to be displayed by others
   * @param intent Intent of the tweet
   * @param polarity Polarity of the tweet
   * @param country Country where tweet was made
   * @return Unit
   */
  private def processResults(intent: Option[Intent], polarity: Int, country: String, wc: Long) = {

    // Match intent and update the database accordingly
    intent match {
      case Some(myIntent: Intent) => myIntent match {
        case TheBuyIntent => {
          if (polarity == 4) buyIntent.pos += 1
          else if (polarity == 0) buyIntent.neg += 1
          else buyIntent.neu += 1
        }
        case TheRecommendIntent => {
          if (polarity == 4) recommendIntent.pos += 1
          else if (polarity == 0) recommendIntent.neg += 1
          else recommendIntent.neu += 1
        }
      }
      case _ => {
        if (polarity == 4) noIntent.pos += 1
        else if (polarity == 0) noIntent.neg += 1
        else noIntent.neu += 1
      }
    }

    // Update countries
    if (countries contains country) countries update(country, countries.getOrElse(country, BigInt(0)) + 1)
    else countries += country -> BigInt(1)

    wordCount += wc
    numTweets += 1

  }

  /**
   * Receive all the incoming messages to this actor
   * @return Nothing
   */
  def receive = {
    // Ingest the reults to be displayed later
    case Result(intent, polarity, country, wc) => processResults(intent, polarity, country, wc)
    // Get Updates
    case GetUdpates => sender ! Updates(buyIntent, recommendIntent, noIntent, getTopCountires(10), running, wordCount, numTweets)
    // Handle unrecognized cases
    case _ => println("The actor InfoManager with ID " + this.toString + " received an unrecognized case.")
  }

}
