package actors

import java.text.Normalizer

import akka.actor.Actor
import akka.util.Timeout
import org.apache.spark.rdd.RDD
import org.apache.spark.streaming.dstream.ReceiverInputDStream
import org.apache.spark.streaming.twitter._
import play.api.Play.current
import play.api.libs.ws._
import twitter4j.Status

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

object Asciifier {

  def apply(string: String): String = {
    var cleaned = string
    for ((unicode, ascii) <- substitutions) {
      cleaned = cleaned.replaceAll(unicode, ascii)
    }

    // convert diacritics to a two-character form (NFD)
    // http://docs.oracle.com/javase/tutorial/i18n/text/normalizerapi.html
    cleaned = Normalizer.normalize(cleaned, Normalizer.Form.NFD)

    // remove all characters that combine with the previous character
    // to form a diacritic.  Also remove control characters.
    // http://docs.oracle.com/javase/6/docs/api/java/util/regex/Pattern.html
    cleaned = cleaned.replaceAll("[\\p{InCombiningDiacriticalMarks}\\p{Cntrl}]", "")

    // size must not change
    //require(cleaned.size == string.size)

    cleaned
  }

  val substitutions = Set(
    (0x00AB, '"'),
    (0x00AD, '-'),
    (0x00B4, '\''),
    (0x00BB, '"'),
    (0x00F7, '/'),
    (0x01C0, '|'),
    (0x01C3, '!'),
    (0x02B9, '\''),
    (0x02BA, '"'),
    (0x02BC, '\''),
    (0x02C4, '^'),
    (0x02C6, '^'),
    (0x02C8, '\''),
    (0x02CB, '`'),
    (0x02CD, '_'),
    (0x02DC, '~'),
    (0x0300, '`'),
    (0x0301, '\''),
    (0x0302, '^'),
    (0x0303, '~'),
    (0x030B, '"'),
    (0x030E, '"'),
    (0x0331, '_'),
    (0x0332, '_'),
    (0x0338, '/'),
    (0x0589, ':'),
    (0x05C0, '|'),
    (0x05C3, ':'),
    (0x066A, '%'),
    (0x066D, '*'),
    (0x200B, ' '),
    (0x2010, '-'),
    (0x2011, '-'),
    (0x2012, '-'),
    (0x2013, '-'),
    (0x2014, '-'),
    (0x2015, '-'),
    (0x2016, '|'),
    (0x2017, '_'),
    (0x2018, '\''),
    (0x2019, '\''),
    (0x201A, ','),
    (0x201B, '\''),
    (0x201C, '"'),
    (0x201D, '"'),
    (0x201E, '"'),
    (0x201F, '"'),
    (0x2032, '\''),
    (0x2033, '"'),
    (0x2034, '\''),
    (0x2035, '`'),
    (0x2036, '"'),
    (0x2037, '\''),
    (0x2038, '^'),
    (0x2039, '<'),
    (0x203A, '>'),
    (0x203D, '?'),
    (0x2044, '/'),
    (0x204E, '*'),
    (0x2052, '%'),
    (0x2053, '~'),
    (0x2060, ' '),
    (0x20E5, '\\'),
    (0x2212, '-'),
    (0x2215, '/'),
    (0x2216, '\\'),
    (0x2217, '*'),
    (0x2223, '|'),
    (0x2236, ':'),
    (0x223C, '~'),
    (0x2264, '<'),
    (0x2265, '>'),
    (0x2266, '<'),
    (0x2267, '>'),
    (0x2303, '^'),
    (0x2329, '<'),
    (0x232A, '>'),
    (0x266F, '#'),
    (0x2731, '*'),
    (0x2758, '|'),
    (0x2762, '!'),
    (0x27E6, '['),
    (0x27E8, '<'),
    (0x27E9, '>'),
    (0x2983, '{'),
    (0x2984, '}'),
    (0x3003, '"'),
    (0x3008, '<'),
    (0x3009, '>'),
    (0x301B, ']'),
    (0x301C, '~'),
    (0x301D, '"'),
    (0x301E, '"'),
    (0xFEFF, ' ')).map { case (unicode, ascii) => (unicode.toChar.toString, ascii.toString) }
}


/**
 * Manages all the tweets
 */
class TweetHandler extends Actor {

  implicit val timeout = Timeout(60 seconds)

  /**
   * Set properties for twitter4J
   */

  val twittercreds = scala.io.Source.fromFile( """./.twittercreds""")
  val creds = twittercreds.getLines.toArray.head.split(" ")
  twittercreds.close()

  val Array(consumerKey, consumerSecret, accessToken, accessTokenSecret) = creds take 4

  System.setProperty("twitter4j.oauth.consumerKey", consumerKey)
  System.setProperty("twitter4j.oauth.consumerSecret", consumerSecret)
  System.setProperty("twitter4j.oauth.accessToken", accessToken)
  System.setProperty("twitter4j.oauth.accessTokenSecret", accessTokenSecret)

  /**
   * End setting properties for twitter4J
   */

  // Parent actof of this actor
  private val parent = infoManager

  // Read the filters from the file
  private val filters = try {
    val filterFile = scala.io.Source fromFile "./filterFile"
    val filter = filterFile.getLines.toArray
    filterFile.close
    if (filter.isEmpty) None else Some(filter)
  } catch {
    case _: Throwable => None
  }

  /**
   * Process every single tweet for some data
   * @param tweet Tweet status
   */
  private def processTweet(tweet: Status): Unit = {

    val text = Asciifier(tweet getText)

    /**
     * Returns the intent of the tweet if one exists
     * @param in The text of the tweet
     * @return Intent of the tweet
     */
    def getIntent(in: String): Option[Intent] = {
      if ((in contains "buy") ||
        (in contains "can I get") ||
        (in contains "one get")) Some(TheBuyIntent)
      else if ((in contains "how is") ||
        (in contains "where can") ||
        (in contains "how can")) Some(TheRecommendIntent)
      else None
    }

    // Get sentiment form the sentiment140 API
    val sentiment: Future[WSResponse] = WS.url("http://www.sentiment140.com/api/classify")
      .withRequestTimeout(1000)
      .withQueryString("text" -> text)
      .get()

    val res = Await.result(sentiment, 60 seconds)
    val polarity = (res.json \ "results" \ "polarity").as[Int]
    val country: String = try {
      tweet.getPlace getCountry
    } catch {
      case _: Throwable => "Unrecognized country"
    }
    val intent = getIntent(text)

    val wc = text.split(" ").filter(!stopWords.contains(_)).length.toLong
    parent ! Result(intent, polarity, country, wc)

  }

  // Twitter stream
  val twitter: ReceiverInputDStream[Status] = filters match {
    case None => TwitterUtils.createStream(ssc, None)
    case Some(filter: Array[String]) => TwitterUtils.createStream(ssc, None, filter)
  }

  twitter foreachRDD {
    (rdd: RDD[Status]) =>
      val tweets: Array[Status] = rdd.collect()
      try {
        tweets foreach processTweet
      } catch {
        case e: Throwable => {
          println("Problems processing tweet with error: " + e.getMessage)
        }
      }
  }

  def start() = {
    ssc.start()
    ssc.awaitTermination()
  }

  def stop() = ssc.stop()

  //private val wordCount = twitter map (_.getText) countByValue 3


  def receive = {
    case StartWorker => this.start()
    case StopWorker => this.stop()
    case _ => println("The actor Tweet handler with ID " + this.toString + " recieved an unrecognized case")
  }

}
