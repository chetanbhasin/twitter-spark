import akka.actor.{ActorSystem, Props}
import org.apache.spark.SparkConf
import org.apache.spark.streaming.{Seconds, StreamingContext}

package object actors {

  case object StartWorker

  case object StopWorker

  case object GetNumWords

  case class Result(intent: Option[Intent], polarity: Int, Country: String, wc: Long)

  trait Intent

  case object TheBuyIntent extends Intent

  case object TheRecommendIntent extends Intent

  val stopWords = List("to", "the", "if", "can",
    "so", "is", "that", "then", "on", "do", "done",
    "at", "above", "a", "an", "below", "havinig", "have",
    "after", "onto", "about", "under", "doing", "very")

  // Setup new spark configuration
  private val conf = new SparkConf()
    .setAppName("SCIA")
    .setMaster("local[4]")
    .set("spark.driver.allowMultipleContexts", "true")
  val ssc = new StreamingContext(conf, Seconds(10))

  val system = ActorSystem("SCIA")
  val infoManager = InfoManager(system, "InformationManager")
  val tweetHandler = system.actorOf(Props[TweetHandler])

}
